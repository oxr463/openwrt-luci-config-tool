# OpenWrt LuCI Config Tool

## Dependencies

- [android-ndk](https://developer.android.com/ndk/downloads)

## Compiling

```sh
ndk-build # build native libraries
ant clean debug # build apk
```

## Installation

```sh
find . -name "*.apk"
./bin/LuCIConfig-debug.apk
./bin/LuCIConfig-debug-unaligned.apk

adb install -r bin/LuCIConfig-debug.apk
```

Note: assumes android device is already connected.

## License

SPDX-License-Identifier: [Apache-2.0](LICENSE)

## Reference

- [ndk-samples/native-activity](https://github.com/android/ndk-samples/tree/android-mk/native-activity)
